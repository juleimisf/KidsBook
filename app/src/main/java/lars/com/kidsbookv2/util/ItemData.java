package lars.com.kidsbookv2.util;

/**
 * Created by eabp on 30/07/2017.
 */

public class ItemData {

    Integer imageId;

    public ItemData(Integer imageId){
        this.imageId = imageId;
    }

    public Integer getImageId(){
        return imageId;
    }
}
