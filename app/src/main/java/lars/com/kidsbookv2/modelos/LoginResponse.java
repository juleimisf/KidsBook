package lars.com.kidsbookv2.modelos;

import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;

/**
 * Created by eabp on 15/07/2017.
 */

public class LoginResponse {

    @SerializedName("login_status")
    @Expose
    private Integer loginStatus;

    @SerializedName("token_api")
    @Expose
    private String tokenApi;

    /**
     *
     * @param tokenApi
     * @param loginStatus
     */
    public LoginResponse (Integer loginStatus, String tokenApi) {
        this.loginStatus = loginStatus;
        this.tokenApi = tokenApi;
    }

    public Integer getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(Integer loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getTokenApi() {
        return tokenApi;
    }

    public void setTokenApi(String tokenApi) {
        this.tokenApi = tokenApi;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "loginStatus=" + loginStatus +
                ", tokenApi='" + tokenApi + '\'' +
                '}';
    }
}


