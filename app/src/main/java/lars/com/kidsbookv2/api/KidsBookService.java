package lars.com.kidsbookv2.api;

import lars.com.kidsbookv2.modelos.LoginResponse;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by eabp on 15/07/2017.
 */

public interface KidsBookService {


    @POST("login")
    Call<LoginResponse> sendLogin(@Query("correo") String correo, @Query("clave") String clave);
}
