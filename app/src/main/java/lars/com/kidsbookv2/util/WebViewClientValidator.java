package lars.com.kidsbookv2.util;

import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * Created by Ezequiel on 27/7/2017.
 */

public class WebViewClientValidator extends WebViewClient{
    private static final String TAG = WebViewClientValidator.class.getSimpleName();
    ProgressBar progressBar;

    public WebViewClientValidator(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }


    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        progressBar.setVisibility(View.INVISIBLE);
        return super.shouldOverrideUrlLoading(view, request);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        progressBar.setVisibility(View.INVISIBLE);
    }



}
