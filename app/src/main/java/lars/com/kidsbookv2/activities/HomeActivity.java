package lars.com.kidsbookv2.activities;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.ButterKnife;
import lars.com.kidsbookv2.R;
import lars.com.kidsbookv2.util.Utils;
import lars.com.kidsbookv2.util.WebViewClientValidator;

public class HomeActivity extends AppCompatActivity{

    private static final String TAG = HomeActivity.class.getSimpleName();
    String token;
    String push;
    String lang;
    int pulse;

    @BindView(R.id.WebView)
    WebView browser;

    @BindView(R.id.load)
    ProgressBar load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Quita la barra de titulo
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        //recibimos los datos enviados de la activity anterior
        token = (String) getIntent().getExtras().getString("token");
        lang = (String) getIntent().getExtras().getString("lang");
        Log.d(TAG, "Parametros recibidos del login: TOKEN="+token+" LANG="+lang);

        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        //Creando la fuente y asignandola a los componentes
        Typeface faceTitulo = Typeface.createFromAsset(getAssets(), "fonts/BandaRegular.ttf");

        //cargando...
        load.setVisibility(View.VISIBLE);
        pulse = 0;

        //Obteniedo el token de firebase asociado a mi dispositivo
        push = FirebaseInstanceId.getInstance().getToken();

        //habilitando el scroll a un webview
        browser.getSettings().setDomStorageEnabled(true);
        browser.setVerticalScrollBarEnabled(true);
        browser.setHorizontalScrollBarEnabled(true);

        browser.getSettings().setUseWideViewPort(true);
        browser.getSettings().setLoadWithOverviewMode(true);
        browser.getSettings().setBuiltInZoomControls(true);
        browser.getSettings().setDisplayZoomControls(false);

        //habilitamos javascript y el zoom
        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setBuiltInZoomControls(true);

        //habilitamos los plugins (flash)
        browser.getSettings().setPluginState(WebSettings.PluginState.ON);

        String changeFontHtml = Utils.changedHeaderHtml("");
        browser.loadDataWithBaseURL(null, changeFontHtml,
                "text/html", "UTF-8", null);

        browser.setWebViewClient(new WebViewClientValidator(load));
        //Se carga el url
        //http://lars.com.co/kidsbook2/public/mb/?token=1234&lang=es&push=1234
        browser.loadUrl("http://lars.com.co/kidsbook2/public/mb/?token="+token+"&lang="+lang+"&push="+push+"");
        Log.d(TAG, "Url: "+browser.getUrl());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN){
            switch (keyCode){
                case KeyEvent.KEYCODE_BACK:
                    if (browser.canGoBack()){
                        browser.goBack();
                    }else {
                        if (pulse == 1) {
                            finish();
                        }else {
                            pulse++;
                            Toast.makeText(this, "Presione atras para salir", Toast.LENGTH_SHORT).show();
                        }
                    }
                    return true;
                default:
                    pulse = 0;
                    break;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
