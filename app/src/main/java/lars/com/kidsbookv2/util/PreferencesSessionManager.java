package lars.com.kidsbookv2.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Ezequiel on 2/8/2017.
 */


public class PreferencesSessionManager {
    private final String TAG = PreferencesSessionManager.class.getSimpleName();
    private static final String KEY_USER_EMAIL = "USER_EMAIL";
    private static final String KEY_USER_PASSWORD = "USER_PASSWORD";
    private static final String KEY_LANGUAGES = "USER_LANGUAGES";
    private static final String KEY_TOKEN = "USER_TOKEN";
    private static final String PREFER_NAME = "PreferencesUser";

    public SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    // Constructor
    public PreferencesSessionManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public void clearPreferences(){
        setUserEmail("");
        setUserPassword("");
        setUserLanguage("");
        setUserToken("");
    }

    public String getUserEmail() {return pref.getString(KEY_USER_EMAIL, "null");}
    public void setUserEmail(String userEmail){
        editor.putString(KEY_USER_EMAIL, userEmail);
        editor.apply();
        editor.commit();
    }

    public String getUserPassword() {return pref.getString(KEY_USER_PASSWORD, "null");}
    public void setUserPassword(String userkey){
        editor.putString(KEY_USER_PASSWORD, userkey);
        editor.apply();
        editor.commit();
    }

    public String getUserLanguage() {return pref.getString(KEY_LANGUAGES, "null");}
    public void setUserLanguage(String userLanguage){
        editor.putString(KEY_LANGUAGES, userLanguage);
        editor.apply();
        editor.commit();
    }

    public String getUserToken() {return pref.getString(KEY_TOKEN, "null");}
    public void setUserToken(String userToken){
        editor.putString(KEY_TOKEN, userToken);
        editor.apply();
        editor.commit();
    }

    @Override
    public String toString() {
        return "PreferencesSessionManager{" +
                "TAG='" + TAG + '\'' +
                "Email='" +getUserEmail() +"\' "+
                "Password='" +getUserPassword() +"\' "+
                "Language='" +getUserLanguage() +"\' "+
                "token='" +getUserToken() +"\' "+
                '}';
    }
}

