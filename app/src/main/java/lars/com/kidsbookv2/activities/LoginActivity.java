package lars.com.kidsbookv2.activities;

import android.content.Intent;
import android.content.Loader;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lars.com.kidsbookv2.R;
import lars.com.kidsbookv2.adapter.SpinnerAdapter;
import lars.com.kidsbookv2.api.KidsBookAdapter;
import lars.com.kidsbookv2.api.KidsBookService;
import lars.com.kidsbookv2.modelos.LoginResponse;
import lars.com.kidsbookv2.util.ItemData;
import lars.com.kidsbookv2.util.PreferencesSessionManager;
import lars.com.kidsbookv2.util.TextValidator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private PreferencesSessionManager preferencesSessionManager;
    private String TAG = this.getClass().getSimpleName();
    private static  KidsBookService kidsBooservice;
    private final int CANTIDAD_CARACTERES = 8;
    private String pruebaToken = "IPRjcF7d1qj01MvQ";
    //hara referencia a las operaciones que se hagan con el teclado
    private InputMethodManager teclado;
    private String languageSelect;
    Integer[] imageArray = { R.drawable.espanol, R.drawable.ingles };

    @BindView(R.id.spinner)
    Spinner language;

    @BindView(R.id.loading)
    ProgressBar load;

    @BindView(R.id.textView_titulo)
    TextView titulo;

    @BindView(R.id.input_nombre)
    EditText input_nombre;

    @BindView(R.id.input_password)
    EditText input_pass;

    @BindView(R.id.button_login)
    Button login;

    @BindView(R.id.restore_password)
    TextView restore;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Quita la barra de titulo
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        //Evitar que el teclado se active al iniciar la app
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //Quitar barra de estado
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);

        //Referencia a Butterknife y usar la declaracion abrev.
        ButterKnife.bind(this);


        //Lenguaje por defecto
        languageSelect = "es";

        ArrayList<ItemData> list=new ArrayList<>();
        list.add(new ItemData(R.drawable.espanol));
        list.add(new ItemData(R.drawable.ingles));

        //Se obtiene la referencia del teclado
        teclado = (InputMethodManager) getSystemService(getBaseContext().INPUT_METHOD_SERVICE);

        //Creando la fuente y asignandola a los componentes
        Typeface faceTitulo = Typeface.createFromAsset(getAssets(), "fonts/BandaRegular.ttf");
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");
        titulo.setTypeface(faceTitulo);
        input_nombre.setTypeface(face);
        input_pass.setTypeface(face);
        login.setTypeface(face);
        restore.setTypeface(face);

        SpinnerAdapter adapter=new SpinnerAdapter(this, R.layout.row, R.id.img, list);
        language.setAdapter(adapter);
        language.setOnItemSelectedListener(this);

        //agregando validaciones sobre la password
        input_pass.addTextChangedListener(new TextValidator(input_pass) {
            @Override
            public void validate(EditText editText, String text) {
                if (text.length() < CANTIDAD_CARACTERES){
                    input_pass.setError("La contraseña es muy corta");
                }
            }
        });

        kidsBooservice = KidsBookAdapter.getApiService();

        preferencesSessionManager = new PreferencesSessionManager(this);
        Log.d(TAG, "Preferencias: "+preferencesSessionManager.toString());

        if (preferencesSessionManager.getUserEmail().compareTo("") != 0 &&
                preferencesSessionManager.getUserEmail().compareTo("null") != 0){

            Log.d(TAG, "Hay preferencias...");
            lanzarHome(preferencesSessionManager.getUserEmail(),
                    preferencesSessionManager.getUserPassword(),
                    preferencesSessionManager.getUserToken(),
                    preferencesSessionManager.getUserLanguage(), true);
        }else{
            Log.d(TAG, "No hay preferencias");
        }
    }


    @OnClick(R.id.button_login)
    public void IntentLogin(final View v){
        //Compara las cajas de texto para verificar que no esten vacias
        if (input_nombre.getText().toString().compareTo("") == 0){
            input_nombre.setError("Escriba su correo");

            //Obtiene el foco
            input_nombre.requestFocus();
        }else if (input_pass.getText().toString().compareTo("") == 0 || input_pass.length() < 8){
            input_pass.setError("Escriba su contraseña");

            //Obtiene el foco
            input_pass.requestFocus();
        }else{
            //Oculta el teclado
            teclado.hideSoftInputFromWindow(input_nombre.getWindowToken(),0);
            teclado.hideSoftInputFromWindow(input_pass.getWindowToken(),0);

            //Muestra el dialogo
            load.setVisibility(View.VISIBLE);

            Call<LoginResponse> call = kidsBooservice.sendLogin(input_nombre.getText().toString(),input_pass.getText().toString());

            call.enqueue(new Callback<LoginResponse>(){

                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.d(TAG, "RESPONSE -> "+response);
                    load.setVisibility(View.INVISIBLE);
                    if (response.body().getLoginStatus() == 2) {
                        if (response.body().getTokenApi() != null && response.body().getTokenApi().compareTo("") != 0) {
                            //Lanza la siguiente activity, false porque no hay preferencias
                            lanzarHome(input_nombre.getText().toString(),
                                    input_pass.getText().toString(),
                                    response.body().getTokenApi(),
                                    languageSelect, false);
                        }else{
                            Snackbar.make(v, "Usuario no existe", Snackbar.LENGTH_SHORT).show();
                            Log.d(TAG, "Respuesta: "+response.body().toString());
                        }
                    }else {
                        Snackbar.make(v, "Usuario Incorrecto", Snackbar.LENGTH_SHORT).show();
                        Log.d(TAG, "Respuesta: "+response.body().toString());
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable throwable) {
                    Log.e(TAG, "*ERROR"+throwable);
                    Snackbar.make(v, "Problemas de Conexion con el Servidor", Snackbar.LENGTH_SHORT).show();
                    load.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    private void lanzarHome(String correo, String pass, String token, String lang, boolean preferences) {
        if (!preferences) {
            Log.d(TAG, "Estableciendo nuevas preferencias");
            preferencesSessionManager.setUserEmail(correo);
            preferencesSessionManager.setUserPassword(pass);
            preferencesSessionManager.setUserLanguage(lang);
            preferencesSessionManager.setUserToken(token);
        }
        Log.d(TAG, "lanzando...");
        Log.d(TAG, "Parametros enviados al Home: TOKEN="+token+" LANG="+lang);
        Intent intent = new Intent(getBaseContext(), HomeActivity.class);
        intent.putExtra("token", token);
        intent.putExtra("lang", lang);
        startActivity(intent);
        this.finish();
    }

    @OnClick(R.id.restore_password)
    public void restorePassword(View v){
        Snackbar.make(v, "Recuperar Clave", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i){
            case 0:
                languageSelect = "es";
                break;
            case 1:
                languageSelect = "en";
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}
}
