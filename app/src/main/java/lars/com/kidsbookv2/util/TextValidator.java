package lars.com.kidsbookv2.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Created by Ezequiel on 17/7/2017.
 * TextValidator para validar las entradas del usuario sobre los campos de textos
 */

public abstract class TextValidator implements TextWatcher{
    private final EditText texto;

    protected TextValidator(EditText texto) {
        this.texto = texto;
    }

    public abstract void validate(EditText editText, String text);

    @Override
    final public void afterTextChanged(Editable s) {
        String text = texto.getText().toString();
        validate(texto, text);
    }

    @Override
    final public void beforeTextChanged(CharSequence s, int start, int count, int after) { /* Don't care */ }

    @Override
    final public void onTextChanged(CharSequence s, int start, int before, int count) { /* Don't care */ }
}

