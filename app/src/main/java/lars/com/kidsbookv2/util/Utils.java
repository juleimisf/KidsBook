package lars.com.kidsbookv2.util;

/**
 * Created by eabp on 22/07/2017.
 */

public class Utils {
    public static String changedHeaderHtml(String htmlText) {

        String head = "<head><meta name=\"viewport\" content=\"width=device-width, user-scalable=yes\" /></head>";

        String closedTag = "</body></html>";
        String changeFontHtml = head + htmlText + closedTag;
        return changeFontHtml;
    }
}
